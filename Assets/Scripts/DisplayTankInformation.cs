using UnityEngine;
using TMPro;
using System;

public class DisplayTankInformation : MonoBehaviour
{
    [SerializeField]
    private TMP_Text information;
    public void DisplayInformation(TankInformation tankInfo)
    {
        information.text = tankInfo.description;
    }
}