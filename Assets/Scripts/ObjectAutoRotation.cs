using UnityEngine;
using UnityEngine.UI;

public class ObjectAutoRotation : MonoBehaviour
{

    [SerializeField]
    private float xAngle, yAngle, zAngle;

    [SerializeField]
    private GameObject model;

    public Toggle toggle;

    private void Awake()
    {
        yAngle = 0.1f;
    }

    private void Update()
    {

        if (toggle.isOn)
        {
            model.transform.Rotate(xAngle, yAngle, zAngle, Space.Self);
        }
        else
        {
            model.transform.Rotate(0, 0, 0, Space.Self);
        }
    }
}