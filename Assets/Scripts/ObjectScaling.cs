using UnityEngine;
using UnityEngine.UI;
public class ObjectScaling : MonoBehaviour {
    [SerializeField]
    private Slider slider;

    [SerializeField]
    private GameObject model;

    private Vector3 scaleChange;

    private void Update() {
        scaleChange = new Vector3(slider.value,slider.value,slider.value);
        model.transform.localScale = scaleChange;
    }
}