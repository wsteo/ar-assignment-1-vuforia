using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tank",menuName = "Tank")]
public class TankInformation : ScriptableObject
{
    public string tankName;
    
    [TextArea(10,100)]
    public string description;
}
