using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    [SerializeField]
    private GameObject scaleAndRotation;

    [SerializeField]
    private GameObject scrollArea;

    [SerializeField]
    private GameObject showDescriptionOn;

    [SerializeField]
    private GameObject showDescriptionOff;

    [SerializeField]
    private Slider slider;

    [SerializeField]
    private ScrollRect _scrollRect;

    [SerializeField]
    private Scrollbar _scrollBar;

    public void enableUI(){
        scaleAndRotation.SetActive(true);
        showDescriptionOn.SetActive(true);
        slider.value = 1;
        //_scrollRect.verticalNormalizedPosition = 1.0f;
    }

    public void disableUI(){
        scaleAndRotation.SetActive(false);
        scrollArea.SetActive(false);
        showDescriptionOn.SetActive(false);
        showDescriptionOff.SetActive(false);
    }
}